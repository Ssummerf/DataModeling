#Modified Conways Game of Life
#Original code from Stephen Davies, CPSC420
#Modifications done by Scott Summerford, 3/25/2018

import numpy as np
import matplotlib.pyplot as plt

def runsim(PROB_POP = .3, SURVIVE = [2,3],BIRTH = [3]):
    WIDTH = 20
    HEIGHT = 20
    NUM_GEN = 100   #Running #
     
    # Return the number of populated neighbors (0-8) of cell x,y on this grid.
    def num_neighbors(grid,x,y):
        neighbors = 0
        if x < WIDTH-1 and grid[x+1,y] == 1:                        # Right
            neighbors = neighbors + 1
        if x > 0 and grid[x-1,y] == 1:                              # Left
            neighbors = neighbors + 1
        if y < HEIGHT-1 and grid[x,y+1] == 1:                       # Up
            neighbors = neighbors + 1
        if y > 0 and grid[x,y-1] == 1:                              # Down
            neighbors = neighbors + 1
        if x < WIDTH-1 and y < HEIGHT-1 and grid[x+1,y+1] == 1:     # Up-right
            neighbors = neighbors + 1
        if x > 0 and y > 0 and grid[x-1,y-1] == 1:                  # Lower-left
            neighbors = neighbors + 1
        if x < WIDTH-1 and y > 0 and grid[x+1,y-1] == 1:            # Lower-right
            neighbors = neighbors + 1
        if x > 0 and y < HEIGHT-1 and grid[x-1,y+1] == 1:           # Upper-left
            neighbors = neighbors + 1
        return neighbors
    
    
    # Return True only if cell x,y should be populated on the generation *after*
    # the one represented by the grid passed.
    def should_be_pop_next_gen(grid,x,y):
        if grid[x,y] == 1:
            if num_neighbors(grid,x,y) in SURVIVE:
                return True
            else:
                return False
        else:
            if num_neighbors(grid,x,y) in BIRTH:
                return True
            else:
                return False
    
    
    # Given a 2d array of 1's and 0's, return a list with the x-coordinates (in a
    # list) and the y-coordinates (in another list) of the cells that are 1's.
    def points_for_grid(grid):
        xcoords = []
        ycoords = []
        for i in range(0,WIDTH):
            for j in range(0,HEIGHT):
                if grid[i,j] == 1:
                    xcoords.append(j)
                    ycoords.append(HEIGHT-i-1)
        return [xcoords,ycoords]
                


    cube = np.empty((WIDTH, HEIGHT, NUM_GEN))
    
    # Create a random starting configuration with about PROB_POP of the cells
    # being initially populated.
    config = np.random.choice([1,0],p=[PROB_POP, 1-PROB_POP],size=WIDTH*HEIGHT)
    config.shape = (WIDTH,HEIGHT)
    cube[:,:,0] = config
    
    
    # Run the simulation.
    for gen in range(1,NUM_GEN):
        for x in range(WIDTH):
            for y in range(HEIGHT):
                if should_be_pop_next_gen(cube[:,:,gen-1],x,y):
                    cube[x,y,gen] = 1
                else:
                    cube[x,y,gen] = 0
    
    #Modified to return the last breadslices sum.
    return cube[:,:,NUM_GEN-1].sum()


#Calculates a parameter sweep based on a set of parameters, and
    # a number of runs, unless otherwise specified
def sweeper(numruns = 50, 
            PROB_POPS = [0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 
                         0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9]):

    runsum = 0
    runmeans = np.zeros(len(PROB_POPS))
    
    #Loop to check each resulting mean for number of runs numruns
    #And for each PROB_POP value
    
    for j in range(0, len(PROB_POPS)):
        for i in range(0, numruns):
            runsum += runsim(PROB_POP = PROB_POPS[j], 
                             SURVIVE =[0,7], BIRTH =[1,6])
        
        runmeans[j] = runsum / numruns
        runsum = 0
    
    #Plotting our results, Prob Pop vs Cells
    plt.clf()
    plt.xlabel("Prob Pop")
    plt.ylabel("Remaining Cells")
    plt.ylim(0, runmeans.max()+1)
    plt.plot(PROB_POPS, runmeans)
    plt.title("Bacterium Oddly Specificum")
    plt.show()