import re
import random


#Loading array, lowercasing all lines
with open('corpus.txt', 'r', errors='replace') as f:
    lines = f.readlines()

#Removing non alphanumeric characters, and indentifiers (l950, l951, l239,  etc.)
for i in range(len(lines)):
    lines[i] = lines[i].lower()
    lines[i] = lines[i].replace("'", "")
    lines[i] = re.sub(r'\W+', ' ', lines[i])
    lines[i] = re.sub(r'\w*\d\w*', '', lines[i])

#Creating a joined string, and formatting out useless strings
space = ""
joined = space.join(lines);

#Splitting into an array, stripping spaces, and empty slots
newlines = joined.split()
newlines = list(filter(None, newlines))

for word in newlines:
    word = word.strip()

#Creating Unigram dictionary
unigram = {}

for word in newlines:
    if word in unigram:
        unigram[word] += 1
    else:
        unigram[word] = 1


#Converting keys / values to a list
keys = list(unigram.keys())
values = list(unigram.values())

#Bigram dictionary generation
#loops through all the words in our list, checks their pairs, creates nested dicts based off pairs
bigrams = {}

for i in range(0, (len(newlines)-1)):
    biword = newlines[i]
    nword = newlines[i+1]
    
    if biword in bigrams:
        if nword in bigrams[biword]:
            bigrams[biword][nword] += 1
        else:
            bigrams[biword][nword] = 1
    else:
        bigrams[biword] = {}
        bigrams[biword][nword] = 1

def gen_Uni():
    outputString = ""
    
    #Generating random string, and formatting it
    
    for i in range(0,100):
        x = str(random.choices(keys,weights=values))
        outputString += x + " " 
    
    outputString = outputString.replace("['", "")
    outputString = outputString.replace("']", "")
    print(outputString)
        

#Generates Bigram string
#Yes I know I can't properly pull strings from lists
#This is my first python program, ever, cut me some slack my dude.
def gen_Bi():
   
    prevString = str(random.choices(keys,weights=values))
    prevString = prevString.replace("['", "")
    prevString = prevString.replace("']", "")
    outputString = prevString + " "
    
    for i in range(0,100):
         bikeys = list(bigrams[prevString].keys())
         bivalues = list(bigrams[prevString].values())
         
         x = str(random.choices(bikeys,weights=bivalues))
         prevString = x
         prevString = prevString.replace("['", "")
         prevString = prevString.replace("']", "")

         outputString += prevString + " "
         
    
    
    print(outputString)


gen_Bi()
        
f.close()