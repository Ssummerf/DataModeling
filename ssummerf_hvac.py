# Program 1 - Modeling Temp Balancing
# Scott Summerford

import numpy as np
import matplotlib.pyplot as plt
import math

delta_t = 5/60          #5 minutes in units of hours
time_values = np.arange(0, 91*24, delta_t)    #3 Months * 30 Days * 24 Hours


#Outside Temp Array Note - Upward line needs to be fixed
sin_values = 6*np.sin(2*math.pi*time_values) + 65
upward_line = time_values * 0.0015	
random_line = np.random.normal(0,.1,len(time_values))
outside_temp = sin_values + random_line.cumsum() + upward_line       #degF

#Inside Thermometer Array (Lows) - Covers inside scheduled temps - All in degF
work = np.repeat(68,9/delta_t)
awake_home = np.repeat(74, 6/delta_t)
sleep_home = np.repeat(70, 9/delta_t)
schedule = np.concatenate([work, awake_home, sleep_home])

#Inside Thermometer Array (Highs)
workH = np.repeat(82,9/delta_t)
awake_homeH = np.repeat(79, 6/delta_t)
sleep_homeH = np.repeat(79, 9/delta_t)
scheduleH = np.concatenate([work, awake_home, sleep_home])

#Tiling each block to fit our model
before_schedule = np.tile(schedule, 37)
before_scheduleH = np.tile(scheduleH, 37)
during_schedule = np.tile(np.repeat(55, 24/delta_t), 14)
during_scheduleH = np.tile(np.repeat(88, 24/delta_t), 14)
after_schedule = np.tile(schedule, 40)
after_scheduleH = np.tile(scheduleH, 40)

#Concantenated Thermostat Arrays
thermostatL = np.concatenate([before_schedule, during_schedule, after_schedule])
thermostatH = np.concatenate([before_scheduleH, during_scheduleH, after_scheduleH])

furnace_rate = 2             # degF/hr
ac_rate = 1.5                #degF / hr
house_leakage_factor = .08   # (degF/hr)/degF
heating_cost_rate = 0.40    #dollars / hour
ac_cost_rate = 0.50         #dollars / hour

heater_on = np.empty(len(time_values))
heater_on[0] = False
ac_on = np.empty(len(time_values))
ac_on[0] = False

#Array of inside temps
T = np.empty(len(time_values))
T[0] = 55

SMIDGE = 1                # for hysterisis factor (degF)

#Flow loop
for i in range(1,len(time_values)):
    
    if heater_on[i-1]:
        if T[i-1] - thermostatL[i-1] > SMIDGE:
            heater_on[i] = False
            furnace_heat = 0              # degF/hr
        else:
            heater_on[i] = True
            furnace_heat = furnace_rate   # degF/hr
    else:
        if T[i-1] - thermostatL[i-1] < -SMIDGE:
            heater_on[i] = True
            furnace_heat = furnace_rate   # degF/hr
        else:
            heater_on[i] = False
            furnace_heat = 0              # degF/hr
            
    if ac_on[i-1]:
        if T[i-1] - thermostatH[i-1] < SMIDGE:
            ac_on[i] = False
            cooling = 0              # degF/hr
        else:
            ac_on[i] = True
            cooling = -ac_rate   # degF/hr
    else:
        if T[i-1] - thermostatH[i-1] > -SMIDGE:
            ac_on[i] = True
            cooling = -ac_rate   # degF/hr
        else:
            ac_on[i] = False
            cooling = 0              # degF/hr

    leakage_rate = (house_leakage_factor * 
        (T[i-1] - outside_temp[i-1]))      # degF/hr

    T_prime = furnace_heat + cooling - leakage_rate

    T[i] = T[i-1] + T_prime * delta_t


#Graph statements
plt.plot(time_values,T,color="brown",label="inside temp",
    linewidth=2,linestyle="-")
plt.plot(time_values, outside_temp,color="blue",
        label="outside temp", linestyle="-")
plt.plot(time_values,5*heater_on,color="purple",label="heater")
plt.plot(time_values,5*ac_on,color="green",label="ac")
plt.plot(time_values,thermostatH, color="orange",label="thermostat high")
plt.plot(time_values,thermostatL, color="gray",label="thermostat low")
plt.legend()
plt.show()

#Sum the total times on the ac/heater on, multiplied by the rate
#Then convert from hours -> month
total = np.round(((heater_on.sum() * heating_cost_rate) + (ac_on.sum()
 * ac_cost_rate)) /(24 * 3), 2)
print("Average monthly cost (heating + ac): $" + str(total))

