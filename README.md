# Compilation of one-off Data Modeling projects

This is a list of Python projects that simulate some aspect of life. Each one takes similar design approaches to different problems. 

Each program swept various paremeters to identify aspects of our model. That might include determining how many contacts an ill person needs to make per day to start an outbreak, or how dense a forest needs to be to start a widespread fire.

Each program also had a write-up regarding those sweeps and their findings. However, in respect to the University I have chosen to keep those private.

## Program 0 - Bigram / Trigram

A very simple program. It takes a corpus of a large size, and creates Bigram/Trigram models that we can use to generate our own sentences with.

## HVAC

Simulates indoor temperature control. It uses a random walk generator to build our temperature model for 3 months, then calculates when to turn the AC/Heating on. 

The main issue we approached was how to create a proper thermostat that doesn't flip on/off too quickly, and that can be adjusted conditionally

## Schelling

Approached the idea of Humans wanting to cluster around similar people (Race, Ethnicity, Etc.)

It created a grid and determined the circumstances where a person would leave if their comfortability exceeded a certain rate. In the model, you'll find that even with a desire for only 2/10 of your neighbors to be like you, it will result in neighborhoods that are extremely homogeneous.

## Fire

Assesses the factors that lead to forest fires by creating a grid of updating generations - essentially a 3D model. 

## Life

A python version of Conway's Game of Life, with added features to simulate how changing the rules affects the end result.

## Ending Remarks

This code was largely by my own design, but some were heavily modified versions of base code provided by Stephen Davies. For additional information regarding methods / design, please read the commented code.