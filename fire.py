#Fire Simulation by Scott Summerford

import numpy as np
import matplotlib.pyplot as plt
import random

def runsim(PROB_LIGHTNING = 0.1, PROB_TREE_IMMUNE = 0.25, 
           PROB_HOUSE_IMMUNE = 0.5, TIME_TO_BURN = 3, 
           FOREST_DENSITY = 0.7, NUM_GEN = 200):
    
    WIDTH = 50
    HEIGHT = 50
    
   
    #Define our numeric represenations of objects
    HOUSE = 7
    BURNING = 30
    KINDLING = 20
    TREE = 1
    EMPTY = 0
    PROB_TREE = 0.5
    
    grid = np.random.choice([TREE,EMPTY],
                                  p=[PROB_TREE, 1-PROB_TREE],size=WIDTH*HEIGHT)
    grid.shape = (WIDTH, HEIGHT)
    
    #Define the size of the corner + the probability of the houses to fill it
    def fill_corner(grid, SQUARE_DIM = 3, PROB = 0.4, H = 7, E = 0):
        HOUSES = np.random.choice([H,E],p=[PROB, 1-PROB],
                                  size=SQUARE_DIM*SQUARE_DIM)
        HOUSES.shape = (SQUARE_DIM, SQUARE_DIM)
        counter = 0
        
        #Set our grids corner
        for row in range(0, SQUARE_DIM):
            for col in range(0, SQUARE_DIM):
                grid[HEIGHT-row-1, WIDTH-col-1] = HOUSES[row,col]
                counter = counter + HOUSES[row,col]
        
        if counter == 0:
            grid[HEIGHT-1, WIDTH-1] = H
                
        
        return grid

     #We're just going to fill the whole grid, as we'll rewrite it with houses   
    
    #Define our random lightning strike coords
    def lightning_strike(grid, PROB_LIGHT = 0.1):
        
        Chance = np.random.choice([1,0], p=[PROB_LIGHT, 1-PROB_LIGHT],
                                  size = 1)
        
        #If we're striking, we're striking
        if Chance[0]:
            L_X = random.randint(0,24)
            L_Y = random.randint(0,49)
            
            if grid[L_X, L_Y] == TREE:
                grid[L_X, L_Y] = BURNING
                
                
        return grid
    
    #Check our Von neighborhood of neighbors to see if they're burning
    def is_neighbor_burning(grid,x,y):
        burning_neighbors = 0
        
        if x < WIDTH-1 and grid[x+1,y] > BURNING - TIME_TO_BURN:              # Right
            burning_neighbors = burning_neighbors + 1
        if x > 0 and grid[x-1,y] > BURNING - TIME_TO_BURN:                  # Left
            burning_neighbors = burning_neighbors + 1
        if y < HEIGHT-1 and grid[x,y+1] > BURNING - TIME_TO_BURN:            # Up
            burning_neighbors = burning_neighbors + 1
        if y > 0 and grid[x,y-1] > BURNING - TIME_TO_BURN:                  # Down
            burning_neighbors = burning_neighbors + 1
        
        return burning_neighbors
    
    def burning_countdown(grid):
        for row in range(0, WIDTH-1):
            for col in range(0, HEIGHT-1):
                if grid[row,col] > 10:
                    grid[row,col] = grid[row,col] - 1
                
                if grid[row,col] < BURNING - TIME_TO_BURN and grid[row,col] > 11:
                    grid[row,col] = 0
        
        return grid
            
    
    
    
    
    #Spreads the fires to adjacent trees. Initializes them as "Kindling"
    #Then loops through again and sets the Kindling flames to Burning
    #This way they can't immediately spread to others
    def fire_spread(grid):
        for row in range(0, WIDTH-1):
            for col in range(0, HEIGHT-1):
                #Burning Tree case
                if is_neighbor_burning(grid,row,col) >0 and grid[row,col] == TREE:
                    grid[row,col] = np.random.choice([TREE,KINDLING],
                        p=[PROB_TREE_IMMUNE, 1-PROB_TREE_IMMUNE], size = 1)[0]
                #Burning House case
                if is_neighbor_burning(grid,row,col) >0 and grid[row,col] == HOUSE:
                    grid[row,col] = np.random.choice([HOUSE,KINDLING],
                        p=[PROB_HOUSE_IMMUNE, 1-PROB_HOUSE_IMMUNE], size = 1)[0]
                    
        for row in range(0, WIDTH-1):
            for col in range(0, HEIGHT-1):
                if grid[row,col] == KINDLING:
                    grid[row,col] = BURNING
        
        return grid
    
    def points_for_grid(grid, forest_type):
        xc = []
        yc = []
        for x in range(WIDTH):
            for y in range(HEIGHT):
                if grid[x,y] == forest_type:
                    xc.append(x)
                    yc.append(y)
                else:
                    if forest_type > 20 and grid[x,y] > BURNING - TIME_TO_BURN:
                        xc.append(x)
                        yc.append(y)
        return [xc,yc]
    
    #For our remaining houses calculation
    def find_houses(grid,forest_type):
        count = 0
        for x in range(WIDTH):
            for y in range(HEIGHT):
                if grid[x,y] == forest_type:
                    count = count + 1
        
        return count
    
    #Start by intializing our 0 gen with a fresh map
    grid = fill_corner(grid)
    cube = np.empty((WIDTH, HEIGHT, NUM_GEN))
    cube[:,:,0] = grid
    
    start_houses = find_houses(grid,HOUSE)
    start_trees = find_houses(grid,TREE)

    
    #Loop through and process each event, then add the new map to the gencube
    for gen in range(1, NUM_GEN):
        grid = lightning_strike(grid, PROB_LIGHT = PROB_LIGHTNING)
        cube[:,:,gen] = grid
        grid = fire_spread(grid)
        grid = burning_countdown(grid)
    
    #Loop through every generation, and plot a graphic of that gens map
    def animate():
        for gen in range(0,NUM_GEN):
            plt.clf()
            xc, yc = points_for_grid(cube[:,:,gen],TREE)
            plt.scatter(xc,yc,marker="^",color="green")
            xc, yc = points_for_grid(cube[:,:,gen],HOUSE)
            plt.scatter(xc,yc,marker="p",color="brown")
            xc, yc = points_for_grid(cube[:,:,gen],BURNING)
            plt.scatter(xc,yc,marker="d",color="orange")
            
            plt.title("Generation #" + str(gen))
            plt.pause(.05)
    
    #Find the fraction of houses remaining, and animate the graph
    #I added Trees too, at the end, for fun.
    end_houses = find_houses(cube[:,:,NUM_GEN-1],HOUSE)
    end_trees = find_houses(cube[:,:,NUM_GEN-1],TREE)
    our_answer = (end_houses / start_houses) * 100
    tree_answer = (end_trees / start_trees) * 100
    
    animate()
    
    print("Remaining Houses: " + str(our_answer) + "%")
    print("Remaining Trees: " + str(tree_answer) + "%")

    
    return our_answer
    

def sweeper(numruns = 25, 
            PROB_VAR = [0.15, 0.3, 0.45, 0.6, 0.75, 0.9, 1]):
    
    #Initialize our list of means for each variable deviation,
    #And the sum to calculate it
    runsum = 0
    runmeans = np.zeros(len(PROB_VAR))
    
    for j in range(0, len(PROB_VAR)):
        for i in range(0, numruns):
            runsum += runsim(PROB_LIGHTNING = PROB_VAR[j])
        
        runmeans[j] = runsum / numruns
        runsum = 0
    
    #Plotting our sweep
    variable_name = "Lighting"
    plt.clf()
    plt.xlabel(variable_name)
    plt.ylabel("% Remaining Houses")
    plt.ylim(0, runmeans.max()+1)
    plt.plot(PROB_VAR, runmeans)
    plt.title(variable_name + " sweep")
    plt.show()


runsim()
    

                
    
                
    
                
    
    
    
    